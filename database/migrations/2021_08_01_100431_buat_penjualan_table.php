<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatPenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualan', function (Blueprint $table) {
            $table->increments('id_penjualan');
            $table->integer('id_member');
            $table->integer('total_item');
            $table->integer('total_harga');
            $table->tinyInteger('diskon')->default(0);
            $table->integer('bayar')->default(0);
            $table->integer('diterima')->default(0);
            $table->integer('id_user');
            $table->string('status')->default('Belum Bayar');
            $table->text('metode_pembayaran')->default('');
            $table->text('mid_transaction_id')->default('');
            $table->text('mid_transaction_status')->default('');
            $table->text('mid_payment_type')->default('');
            $table->text('mid_order_id')->default('');
            $table->text('mid_signature_key')->default('');
            $table->text('mid_token')->default('');
            $table->text('mid_gross_amount')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualan');
    }
}
