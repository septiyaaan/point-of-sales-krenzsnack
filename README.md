<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Project Point Of Sales Krenzsnack

Salam sehat selalu untuk kita semua, project ini adalah project yang di bangun dengan framework laravel yang bertujuan untuk project pribadi dan sekaligus untuk bahan review HR Perusahaan sebagaimana mestinya di gunakan untuk pertimbangan proses recruitmen 

Pada Fitur Aplikasi Point Of Sales ini, adalah 
1. fitur management user ( member langganan )
    - Cetak kartu Member
    - Database Detail Kontak Member
2. Management Stock 
    - In dan Out History Stock
    - Traking Produk terlaris berdasarkan waktu tertentu
    - Pembelian terhadap supplier
3. Management Bisnis
    - Cashflow Keuangan Perusahan
    - Kredit dan Debit Transaksi Bisnis
    - Report Laba Rugi Bisnis
4. Mesin Kasir 
    - Tersedia pembayaran secara tunai
    - Tersedia Pembayaran secara transfer menggunakan Payment Gateway

## Cara Instalasi Project Point Of Sales
1. Clone Repository
2. Copy dan Setup .env Project
3. php artisan key:generate
4. php artisan migrate --seed
5. php artisan serve

## login menggunakan user yang sudah saya sediakan pada migrasi database
username : septiyandwinugroho98@gmail.com
password : silahkan_hubungi_saya
